<!DOCTYPE HTML>

	<head>
		<title>My Resume Website</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />

	</head>
	<body>

		<!-- Header -->
			<header id="header" class="alt">
				<div class="logo"><a href="index.html">HEY!! <span> what's popin??</span></a></div>
				<a href="#menu">Menu</a>
			</header>

		<!-- Nav -->
			<nav id="menu">
				<ul class="links">
					<li><a href="index.html">Home</a></li>
					<li><a href="RexorResume.html">Resume</a></li>
					
				</ul>
			</nav>

		<!-- Banner -->
			<section class="banner full">
				<article>
					<img src="images/3.jpg" alt="" />
					<div class="inner">
						<header>
							<p>A responsive resume website by </p>
							<h2>Rexor Gutierrez</h2>
						</header>
					</div>
				</article>
				<article>
					<img src="images/slide03.jpg" alt="" />
					<div class="inner">
						<header>
							<p>University of Southeastern Philippines</p>
							<h2>Institute of Computing</h2>
						</header>
					</div>
				</article>
				<article>
					<img src="images/7.jpg"  alt="" />
					<div class="inner">
						<header>
							<p>Bachelor of Science in Information Technology</p>
							<h2>Application System Development</h2>
						</header>
					</div>
				</article>
				
			</section>

		<!-- One -->


			<section id="one" class="wrapper style2">
				<div class="inner">
					<div class="grid-style">

						<div>
							<div class="box">
 								<div class="content">
									<header class="align-center">
										<p>Discover my narcissism by my superiority</p>
										<h2>ABOUT ME</h2>
									</header>
									<p> Hi , I'm Rexor Gutierrez. A 3rd year student of University of Southeastern Philippines taking Bachelor of Science in Technology. I live here somewhere in Davao City. I love playing outdoor games like Frisbee , Soccer and Basketball and also I play video games. My favorited video games are Dota2 , Player Unkown BattleGrounds etc. Im a nocturnal type of person,I'am more than active doing things at night than a day. I love chiling hanging with friends and do some stupid things. I love coffee everytime I study and coding. If you want to know me more see me in person :) </p>
									<footer class="align-center">
										<a href="RexorResume.html" class="button alt">MY RESUME</a>
									</footer>
								</div>
							</div>
						</div>
					<div>
				</div>
			</section>

		<!-- Two -->
			<section id="two" class="wrapper style3">
				<div class="inner">
					<header class="align-center">
						<p>INFORMATION TECHNOLOGY</p>
						<h2>Application System Development</h2>
					</header>
				</div>
			</section>

		<!-- Three -->
			<section id="three" class="wrapper style2">
			

			<footer id="footer">
				<div class="container">
					<ul class="icons">
						<li><a href="https://twitter.com/_RexorGutierrez?lang=en" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="https://www.facebook.com/profile.php?id=100001408738967" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="https://www.instagram.com/unxpctedrexor/" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="https://mail.google.com/mail/u/0/#inbox" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
					</ul>
				</div>
				<div class="copyright">
					&copy; Copyright 2018. All rights reserved. Created by RexorGutierrez
				</div>
			</footer>
		</section>
		
		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>